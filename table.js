let extract = [{conta: 'Uber', valor: 1430, mes: 12},
{conta: 'PS Store', valor: 40, mes: 3},
{conta: 'GPlay', valor: 37.5, mes: 3},
{conta: 'Uber', valor: 1382, mes: 3},
{conta: 'iFood', valor: 77, mes: 2},
{conta: 'Uber', valor: 3, mes: 1},
{conta: 'iFood', valor: 20.34, mes: 1},
{conta: 'iFood', valor: 37.40, mes: 1},
{conta: 'Uber', valor: 17.70, mes: 3},
{conta: 'GPlay', valor: 16.70, mes: 3},
{conta: 'AppStore', valor: 15, mes: 2},
{conta: 'Uber', valor: 96.11, mes: 2},
{conta: 'GPlay', valor: 7.71, mes: 2},
{conta: 'AppStore', valor: 3.33, mes: 2},
{conta: 'iFood', valor: 47.98, mes: 3},
{conta: 'AppStore', valor: 12.22, mes: 3},
{conta: 'Uber', valor: 2.56, mes: 4},
{conta: 'Uber', valor: 5.32, mes: 3},
{conta: 'PS Store', valor: 5.44, mes: 3},
{conta: 'PS Store', valor: 98.37, mes: 3},
{conta: 'PS Store', valor: 78.90, mes: 1},
{conta: 'GPlay', valor: 65.03, mes: 3},
{conta: 'iFood', valor: 32.12, mes: 3},
{conta: 'Uber', valor: 34.56, mes: 1},
{conta: 'iFood', valor: 1480, mes: 3},
{conta: 'Uber', valor: 5.34, mes: 3},
{conta: 'iFood', valor: 6.12, mes: 1},
{conta: 'Uber', valor: 344.12, mes: 3},
{conta: 'GPlay', valor: 96.10, mes: 3},
{conta: 'Uber', valor: 6.09, mes: 1},
{conta: 'PS Store', valor: 3.21, mes: 3}];
let months = [
	{id:1, value:'Janeiro'},
	{id:2, value:'Fevereiro'},
	{id:3, value:'Março'},
	{id:4, value:'Abril'},
	{id:5, value:'Maio'},
	{id:6, value:'Junho'},
	{id:7, value:'Julho'},
	{id:8, value:'Agosto'},
	{id:9, value:'Setembro'},
	{id:10, value:'Outubro'},
	{id:11, value:'Novembro'},
	{id:12, value:'Dezembro'}
];
	
print_table();

print_table_consolidated();


function print_table(){
	let tableRef = document.getElementById('tb_fatura').getElementsByTagName('tbody')[0];
	
	extract.forEach(item => {
		let newRow = tableRef.insertRow();
		
		print_cell(0, newRow, item['conta']);
		print_cell(1, newRow, 'R$ ' + formatMoney(item['valor'],2,',','.'));
		print_cell(2, newRow, format_month(item['mes']));
	});
}

function print_table_consolidated() {
	let consolidated = get_consolidated();
	let tableRef = document.getElementById('tb_fatura_consolidado').getElementsByTagName('tbody')[0];
	
	let total = 0;

	consolidated.forEach(item => {
		let newRow = tableRef.insertRow();
		
		total = total + item['value'];
		
		print_cell(0, newRow, item['mes']);
		print_cell(1, newRow, 'R$ ' + formatMoney(item['value'],2,',','.'));
	});
	
	create_total(total);
}

function create_total(total){

	let tableReftfoot = document.getElementById('tb_fatura_consolidado').getElementsByTagName('tfoot')[0];
	let newRow = tableReftfoot.insertRow();
	let th = document.createElement('th');
	th.innerHTML = "Total:";
	th.rowspan = 1;
	newRow.appendChild(th);	
	print_cell(1, newRow, 'R$ ' + formatMoney(total,2,',','.'));
}

function get_consolidated(){
	let consolidated = [];
	
	months.forEach(month => {
		value_final = 0;
		extract.filter(x=> x.mes === month.id).forEach(value => {
			value_final = value.valor + value_final;
		});
		
		if(value_final > 0){
			consolidated.push({'mes': month.value, 'value': value_final})
		
		}
	});
	
	return consolidated;
}


function format_month(month_select){
	
	month = months.find(x=> x.id === month_select);
	
	if(month){
		return month.value;
	} else {
		return '';
	}
}

function print_cell(id, row, value){
	let newCell  = row.insertCell(id);
	let newText  = document.createTextNode(value);
	newCell.appendChild(newText);	
}

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
  try {
	decimalCount = Math.abs(decimalCount);
	decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

	const negativeSign = amount < 0 ? "-" : "";

	let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
	let j = (i.length > 3) ? i.length % 3 : 0;

	return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
	console.log(e)
  }
};

